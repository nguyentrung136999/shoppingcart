import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from './Product';
@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private httpClient: HttpClient) { }
  getProducts(): Observable<HttpResponse<Product[]>> {
    const apiUrl = 'https://steelsoftware.azurewebsites.net/api/FresherFPT';
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    const result = this.httpClient.get<Product[]>(apiUrl, options);
    result.subscribe(results => console.log(results));
    return result;
  }

 

  addOrder(formData: any) {
    const data = JSON.stringify(formData);
    const apiUrl = 'https://steelsoftware.azurewebsites.net/api/FresherFPT/CheckOut';
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.post(apiUrl, data, options);
  }
}
