import { Component } from '@angular/core';
import { Product } from './Service/Product'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductService } from './Service/product.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ShoppingCart';
  products:any;
  productsAdd:Product[]=[];
  showShopCart:boolean=false;
  
  constructor(private productService:ProductService){
    
  }
   
   ngOnInit(){
     this.getProducts();
   }

  getProducts(){
   
    this.productService.getProducts().subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.products=res.body;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }

  addToCart(product: any){
    var obj={
      id:product.id,
      productName:product.productName,
      price:product.price,
      promotionPrice:product.promotionPrice,
      quantity:1,
      image:product.image
    };
    let index=this.productsAdd.findIndex(x=>x.id==obj.id);
    if(index==-1)
      this.productsAdd.push(obj as Product);
    else 
      this.productsAdd[index].quantity++;
    alert("Đã thêm vào shopping cart");
  }
}
