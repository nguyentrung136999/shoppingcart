import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Product } from '../Service/Product';
import { ProductService } from '../Service/product.service';

@Component({
  selector: 'app-shopcart',
  templateUrl: './shopcart.component.html',
  styleUrls: ['./shopcart.component.css']
})
export class ShopcartComponent implements OnInit {
  @Input('productsAdd') productsAdded!:Product[];
  sumMoney=0;
  form:FormGroup;
  successSubmit=false;

  constructor(private fb:FormBuilder,private productService:ProductService) {
    this.form=this.fb.group(
      {
          userName:['',Validators.required],
          phoneNumber:['',Validators.required],
          address:['',Validators.required],
      }
    )
   }
   get userName() { return this.form.get('userName'); }

   get  phoneNumber() { return this.form.get('phoneNumber'); }

   get  address() { return this.form.get('address'); }

  
  ngOnInit(): void {
    this.updateSumMoney();
  }
  updateQuantity(id:any,quantity:any){
    this.productsAdded.forEach(element => {
        if(element.id==id){
          element.quantity=quantity;
        }
    });
    this.updateSumMoney();
  }
  updateSumMoney() {
    this.sumMoney=0;
    for(let item of this.productsAdded) {
      this.sumMoney+=(item.price*item.quantity)/100*(100-item.promotionPrice);
    }
  }
  removeProduct(item:Product){
      let index=this.productsAdded.findIndex(x=>x.id==item.id);
      this.productsAdded.splice(index,1);
      this.updateSumMoney()
  }
  onSubmit(){
    let data={
      userName: this.userName?.value,
      phoneNumber: this.phoneNumber?.value,
      address: this.address?.value,
      products: this.productsAdded,
    }
    this.productService.addOrder(data).subscribe(res=>{
      // console.log(this.form.value),
      // console.log(res);
      if(res.status==200){
        this.successSubmit=true;
      }
      else{
        alert('Failed!');
      }
    })
  }
}
